# Unity Transform clipboard

An Unity editor utility to copy the *Transform* component to Windows clipboard in TEXT format and paste anywhere.

---

##To Copy
1. Click the gear button on *Transform* component and "Copy Data".
2. In Popup window, select field(s) to copy and click button "Copy".
Paste in text format.

##To Paste
Click the gear button on *Transform* component and "Paste Data".

If the data is in *KnownTextFormat*, the text data can be assigned back to *Transform* component.

---

##**KnownTextFormat** of data is:
**position=DATA,rotation=DATA,scale=DATA;**