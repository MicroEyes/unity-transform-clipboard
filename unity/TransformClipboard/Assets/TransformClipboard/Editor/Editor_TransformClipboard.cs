﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using UnityEditor;
using UnityEngine;

public class Editor_TransformClipboard
{
    #region Windows functions
    [DllImport("User32.dll", SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    private static extern bool IsClipboardFormatAvailable(uint format);

    [DllImport("User32.dll", SetLastError = true)]
    private static extern IntPtr GetClipboardData(uint uFormat);

    [DllImport("User32.dll", SetLastError = true)]
    private static extern IntPtr SetClipboardData(uint uFormat, IntPtr data);

    [DllImport("User32.dll", SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    private static extern bool EmptyClipboard();

    [DllImport("User32.dll", SetLastError = true, CharSet = CharSet.Ansi)]
    private static extern uint RegisterClipboardFormat(string a_structName);

    [DllImport("User32.dll", SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    private static extern bool OpenClipboard(IntPtr hWndNewOwner);

    [DllImport("User32.dll", SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    private static extern bool CloseClipboard();

    [DllImport("Kernel32.dll", SetLastError = true)]
    private static extern IntPtr GlobalLock(IntPtr hMem);

    [DllImport("Kernel32.dll", SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    private static extern bool GlobalUnlock(IntPtr hMem);

    [DllImport("Kernel32.dll", SetLastError = true)]
    private static extern int GlobalSize(IntPtr hMem);
    #endregion
    
    const string FORMAT__VECTOR_TO_STRING = "0.######";
    const char SEPARATOR_FIELD = ' ';
    const char SEPARATOR_FIELD_VALUE = '=';
    const char SEPARATOR_VALUE_VECTOR = ',';
    const string FIELD_NAME__POSITION = "position";
    const string FIELD_NAME__ROTATION = "rotation";
    const string FIELD_NAME__SCALE = "scale";
    private const uint CF_UNICODETEXT = 13U;

    /// <summary>
    /// Popup window size.
    /// </summary>
    static readonly Vector2 RECT_WINDOW_SIZE = new Vector2(170, 80);

    /// <summary>
    /// Popup Window position offset from InspectorWindow xMax, yMax.
    /// </summary>
    static readonly Vector2 RECT_WINDOW_POSITION_OFFSET = new Vector2(0, 65);

    /// <summary>
    /// Contains all possible combination formats for Position, Rotation, Scale.
    /// </summary>
    static readonly Dictionary<int, string> s_dictTransformDataFormats = new Dictionary<int, string>()
    {
        { 001, FIELD_NAME__SCALE + "=\"{2}\""},
        { 010, FIELD_NAME__ROTATION + "=\"{1}\""},
        { 011, FIELD_NAME__ROTATION + "=\"{1}\"" + SEPARATOR_FIELD + FIELD_NAME__SCALE + "=\"{2}\""},
        { 100, FIELD_NAME__POSITION + "=\"{0}\""},
        { 101, FIELD_NAME__POSITION + "=\"{0}\"" + SEPARATOR_FIELD + FIELD_NAME__SCALE + "=\"{2}\""},
        { 110, FIELD_NAME__POSITION + "=\"{0}\"" + SEPARATOR_FIELD + FIELD_NAME__ROTATION + "=\"{1}\""},
        { 111, FIELD_NAME__POSITION + "=\"{0}\"" + SEPARATOR_FIELD + FIELD_NAME__ROTATION + "=\"{1}\"" + SEPARATOR_FIELD + FIELD_NAME__SCALE + "=\"{2}\""},
    };

    #region Clipboard functions

    /// <summary>
    /// Callback from Popup Window, when toggle selected.
    /// </summary>
    /// <param name="a_transform">Target Transform Ref.</param>
    /// <param name="a_selectedStates">Selected state to format.</param>
    static void OnSelected(Transform a_transform, bool[] a_selectedStates)
    {
        int l_result = (a_selectedStates[0] ? 100 : 0) + (a_selectedStates[1] ? 10 : 0) + (a_selectedStates[2] ? 1 : 0);

        string l_format = s_dictTransformDataFormats[l_result];
        string l_StrSrc = string.Format(
            l_format, Vector3ToString(a_transform.localPosition),
            Vector3ToString(a_transform.localRotation.eulerAngles),
            Vector3ToString(a_transform.localScale));

        Internal_CopyData(l_StrSrc);
    }

    [MenuItem("CONTEXT/Transform/Copy", priority = 201)]
    static void CopyData(MenuCommand menuCommand)
    {
        //Getting inspector window dimension on screen.
        var windowType = typeof(Editor).Assembly.GetType("UnityEditor.InspectorWindow");
        EditorWindow l_inspectorWindow = EditorWindow.GetWindow(windowType);
        
        //Creating Rect dimension for Popup Window.
        Rect l_rect = new Rect(l_inspectorWindow.position.xMax - RECT_WINDOW_SIZE.x - RECT_WINDOW_POSITION_OFFSET.x, l_inspectorWindow.position.y + RECT_WINDOW_POSITION_OFFSET.y, RECT_WINDOW_SIZE.x, RECT_WINDOW_SIZE.y);        
        EditorWindow_TransformClipboard l_obj = new EditorWindow_TransformClipboard(l_rect, menuCommand.context as Transform, OnSelected);

        //Added try{}catch intentionally because PopupWindow.Show was throwing unknown 'ExitGUIException'.
        try
        {
            PopupWindow.Show(l_rect, l_obj);
        }
        catch (ExitGUIException ex) { /*nothing to do here*/ }
    }

    static void Internal_CopyData(string a_StrSrc)
    {
        try
        {
            Debug.Log("Text to copy '" + a_StrSrc + "'");
            if (!OpenClipboard(IntPtr.Zero))
                throw new Exception("Unable to open clipboard");

            if (!EmptyClipboard())
                throw new Exception("Unable to empty clipboard");


            byte[] l_bytes = Encoding.Unicode.GetBytes(a_StrSrc + "\0");
            int l_bytesToAllocate = l_bytes.Length * sizeof(byte);

            IntPtr l_ptrTarget = Marshal.AllocHGlobal(l_bytesToAllocate);
            if (l_ptrTarget == IntPtr.Zero)
                throw new Exception("Unable to allocate memory for source data.");

            l_ptrTarget = GlobalLock(l_ptrTarget);
            if (l_ptrTarget == IntPtr.Zero)
                throw new Exception("Unable to get global lock.");

            Marshal.Copy(l_bytes, 0, l_ptrTarget, l_bytesToAllocate);

            if (!GlobalUnlock(l_ptrTarget))
            {
                throw new Exception("Unable to unlock global lock.");
            }

            l_ptrTarget = SetClipboardData(CF_UNICODETEXT, l_ptrTarget);
            if (l_ptrTarget != IntPtr.Zero)
                Debug.Log("Copy data finished succussfully");
            else
                Debug.LogError("Unable to set clipboard data");
        }
        catch (Exception e)
        {
            Debug.LogException(e);
        }
        finally
        {
            if (!CloseClipboard())
                Debug.LogError("Unable to close clipboard");
        }
    }

    /// <summary>
    /// Paste clipboard data in 'Transform' component.
    /// </summary>
    /// <param name="menuCommand"></param>
    [MenuItem("CONTEXT/Transform/Paste", priority = 202)]
    static void PasteData(MenuCommand menuCommand)
    {
        Debug.Log("Paste data started");
        Transform l_transform = menuCommand.context as Transform;

        try
        {
            if (!OpenClipboard(IntPtr.Zero))
            {
                throw new Exception("Unable to open clipboard");
            }

            IntPtr l_nativePtrSaved = GetClipboardData(CF_UNICODETEXT);
            if (l_nativePtrSaved != IntPtr.Zero)
            {
                string l_strData = string.Empty;
                var lpwcstr = GlobalLock(l_nativePtrSaved);
                if (lpwcstr != IntPtr.Zero)
                {
                    l_strData = Marshal.PtrToStringUni(l_nativePtrSaved);
                    if (!GlobalUnlock(lpwcstr))
                        throw new Exception("Unable to unlock global lock.");
                }
                else
                {
                    throw new Exception("Unable to get global lock.");
                }

                Debug.Log("Clipboard data '" + l_strData + "'");
                UnityEditor.Undo.RecordObject(menuCommand.context, "Copy component");
                AssignData(l_transform, l_strData);
            }
            else
            {
                throw new Exception("Unable to get clipboard data");
            }

        }
        catch (Exception e)
        {
            Debug.LogException(e);
        }
        finally
        {
            if (!CloseClipboard())
            {
                Debug.LogError("Unable to close clipboard");
            }
        }

        Debug.Log("Paste data end.");
    }
    #endregion

    #region Helper functions

    /// <summary>
    /// Converts Vector to string.
    /// </summary>
    /// <param name="a_vector">Vector to convert.</param>
    /// <returns></returns>
    static string Vector3ToString(Vector3 a_vector)
    {
        return
            a_vector.x.ToString(FORMAT__VECTOR_TO_STRING) + SEPARATOR_VALUE_VECTOR +
            a_vector.y.ToString(FORMAT__VECTOR_TO_STRING) + SEPARATOR_VALUE_VECTOR +
            a_vector.z.ToString(FORMAT__VECTOR_TO_STRING);
    }

    /// <summary>
    /// Converts String to Vector3.
    /// </summary>
    /// <param name="a_vectorStr">String to convert.</param>
    /// <returns></returns>
    static Vector3 StringToVector3(String a_vectorStr)
    {
        string[] l_arrStrVec = a_vectorStr.Split(SEPARATOR_VALUE_VECTOR);
        Vector3 l_vec = new Vector3(
            Convert.ToSingle(l_arrStrVec[0]),
            Convert.ToSingle(l_arrStrVec[1]),
            Convert.ToSingle(l_arrStrVec[2])
            );
        return l_vec;
    }

    /// <summary>
    /// Assigning clipboard string to 'Transform' component.
    /// </summary>
    /// <param name="a_targetTransform">Transform reference to assign data.</param>
    /// <param name="a_data">Data string to assign.</param>
    static void AssignData(Transform a_targetTransform, string a_data)
    {
        //Spliting different fields with FIELD SEPARATOR.
        string[] l_arrFieldWithData = a_data.Split(SEPARATOR_FIELD);

        //Looping all fields with value.
        for (int l_indexField = 0; l_indexField < l_arrFieldWithData.Length; l_indexField++)
        {
            string l_fieldWithData = l_arrFieldWithData[l_indexField];

            //Splitting field with value.
            string[] l_arrFieldAndData = l_fieldWithData.Split(SEPARATOR_FIELD_VALUE);

            string l_fieldName = l_arrFieldAndData[0];

            //Replacing " in VALUE string.
            string l_data = l_arrFieldAndData[1].Replace("\"", string.Empty);

            //Checking and assigning field value.
            switch (l_fieldName)
            {
                case FIELD_NAME__POSITION:
                    a_targetTransform.localPosition = StringToVector3(l_data);
                    break;
                case FIELD_NAME__ROTATION:
                    a_targetTransform.localRotation = Quaternion.Euler(StringToVector3(l_data));
                    break;
                case FIELD_NAME__SCALE:
                    a_targetTransform.localScale = StringToVector3(l_data);
                    break;
                default:
                    throw new Exception("Unknown property '" + l_fieldName + "'");
            }
        }
    }

    #endregion
}