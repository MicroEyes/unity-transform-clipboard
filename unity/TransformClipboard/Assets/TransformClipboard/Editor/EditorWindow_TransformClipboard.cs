﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEditor;
using UnityEngine;

public class EditorWindow_TransformClipboard : PopupWindowContent
{
    Rect m_winRect;

    bool[] m_selectedStates = new bool[3];
    Action<Transform, bool[]> m_onSelected = null;
    Transform m_transform;

    public EditorWindow_TransformClipboard(Rect a_winRect, Transform a_transform, Action<Transform, bool[]> a_onSelected)
    {
        m_winRect = a_winRect;
        m_onSelected = a_onSelected;
        m_transform = a_transform;
    }

    public override void OnOpen()
    {
        for (int l_index = 0; l_index < m_selectedStates.Length; l_index++)
        {
            m_selectedStates[l_index] = true;
        }
    }
    public override void OnGUI(Rect rect)
    {
        editorWindow.position = m_winRect;

        m_selectedStates[0] = EditorGUILayout.Toggle("Position", m_selectedStates[0]);
        m_selectedStates[1] = EditorGUILayout.Toggle("Rotation", m_selectedStates[1]);
        m_selectedStates[2] = EditorGUILayout.Toggle("Scale", m_selectedStates[2]);

        bool l_lastEnabled = GUI.enabled;

        GUI.enabled = l_lastEnabled & !IsAllDisabled();
        if (GUILayout.Button("Copy"))
        {
            m_onSelected.Invoke(m_transform, m_selectedStates);
            editorWindow.Close();
        }
        GUI.enabled = l_lastEnabled;
    }

    bool IsAllDisabled()
    {
        foreach (bool l_value in m_selectedStates)
        {
            if (l_value)
                return false;
        }

        return true;
    }

    public override Vector2 GetWindowSize()
    {
        return new Vector2(m_winRect.width, m_winRect.height);
    }

}